﻿/*
 Wrap Interface of Kinect2 SDK
 According to MS Kinect SDK Vesrion 2.0.1410.19000

 Date : 2014/12/3
 Version : 0.5
 
The MIT License (MIT)

Copyright (c) 2014 DavidWTF

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;

namespace Kinect2
{
    namespace Face
    {
        using WAITABLE_HANDLE = System.IntPtr;
        using TIMESPAN = System.Int64;
        using HRESULT = System.Int32;
        using DWORD = System.UInt32;

        public class KinectFace
        {
            [DllImportAttribute("Kinect20.Face")]
            public static extern HRESULT CreateFaceFrameSource(
                [In, MarshalAs(UnmanagedType.Interface)] IKinectSensor sensor,
                [In] UInt64 initialTrackingId,
                [In] uint initialFaceFrameFeatures,
                [Out, MarshalAs(UnmanagedType.Interface)] out IFaceFrameSource source);

            [DllImportAttribute("Kinect20.Face")]
            public static extern HRESULT CreateHighDefinitionFaceFrameSource(
                [In, MarshalAs(UnmanagedType.Interface)] IKinectSensor sensor,
                [Out, MarshalAs(UnmanagedType.Interface)] out IHighDefinitionFaceFrameSource hdFaceFrameSource);

            [DllImportAttribute("Kinect20.Face")]
            public static extern HRESULT CreateFaceAlignment(
                [Out, MarshalAs(UnmanagedType.Interface)] out IFaceAlignment faceAlignment);

            [DllImportAttribute("Kinect20.Face")]
            public static extern HRESULT CreateFaceModel(
                [In] float scale,
                [In] UInt32 capacity,
                [In, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] deformations,
                [Out, MarshalAs(UnmanagedType.Interface)] out IFaceModel faceModel);

            [DllImportAttribute("Kinect20.Face")]
            public static extern HRESULT GetFaceModelVertexCount(
                [Out] out UInt32 vertexCount);

            [DllImportAttribute("Kinect20.Face")]
            public static extern HRESULT GetFaceModelTriangleCount(
                [Out] out UInt32 triangleCount);

            [DllImportAttribute("Kinect20.Face")]
            public static extern HRESULT GetFaceModelTriangles(
                [In] UInt32 capacity,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] UInt32[] triangeVertices);
        }

        public enum FacePointType
        {
            None = -1,
            EyeLeft = 0,
            EyeRight = 1,
            Nose = 2,
            MouthCornerLeft = 3,
            MouthCornerRight = 4,
            Count = (MouthCornerRight + 1)
        };

        public enum FaceFrameFeatures
        {
            None = 0,
            BoundingBoxInInfraredSpace = 0x1,
            PointsInInfraredSpace = 0x2,
            BoundingBoxInColorSpace = 0x4,
            PointsInColorSpace = 0x8,
            RotationOrientation = 0x10,
            Happy = 0x20,
            RightEyeClosed = 0x40,
            LeftEyeClosed = 0x80,
            MouthOpen = 0x100,
            MouthMoved = 0x200,
            LookingAway = 0x400,
            Glasses = 0x800,
            FaceEngagement = 0x1000
        };

        public enum FaceProperty
        {
            Happy = 0,
            Engaged = 1,
            WearingGlasses = 2,
            LeftEyeClosed = 3,
            RightEyeClosed = 4,
            MouthOpen = 5,
            MouthMoved = 6,
            LookingAway = 7,
            Count = (LookingAway + 1)
        };

        public enum HighDetailFacePoints
        {
            LefteyeInnercorner = 210,
            LefteyeOutercorner = 469,
            LefteyeMidtop = 241,
            LefteyeMidbottom = 1104,
            RighteyeInnercorner = 843,
            RighteyeOutercorner = 1117,
            RighteyeMidtop = 731,
            RighteyeMidbottom = 1090,
            LefteyebrowInner = 346,
            LefteyebrowOuter = 140,
            LefteyebrowCenter = 222,
            RighteyebrowInner = 803,
            RighteyebrowOuter = 758,
            RighteyebrowCenter = 849,
            MouthLeftcorner = 91,
            MouthRightcorner = 687,
            MouthUpperlipMidtop = 19,
            MouthUpperlipMidbottom = 1072,
            MouthLowerlipMidtop = 10,
            MouthLowerlipMidbottom = 8,
            NoseTip = 18,
            NoseBottom = 14,
            NoseBottomleft = 156,
            NoseBottomright = 783,
            NoseTop = 24,
            NoseTopleft = 151,
            NoseTopright = 772,
            ForeheadCenter = 28,
            LeftcheekCenter = 412,
            RightcheekCenter = 933,
            Leftcheekbone = 458,
            Rightcheekbone = 674,
            ChinCenter = 4,
            LowerjawLeftend = 1307,
            LowerjawRightend = 1327
        };

        public enum FaceShapeAnimations
        {
            JawOpen = 0,
            LipPucker = 1,
            JawSlideRight = 2,
            LipStretcherRight = 3,
            LipStretcherLeft = 4,
            LipCornerPullerLeft = 5,
            LipCornerPullerRight = 6,
            LipCornerDepressorLeft = 7,
            LipCornerDepressorRight = 8,
            LeftcheekPuff = 9,
            RightcheekPuff = 10,
            LefteyeClosed = 11,
            RighteyeClosed = 12,
            RighteyebrowLowerer = 13,
            LefteyebrowLowerer = 14,
            LowerlipDepressorLeft = 15,
            LowerlipDepressorRight = 16,
            Count = (LowerlipDepressorRight + 1)
        };

        public enum FaceShapeDeformations
        {
            PCA01 = 0,
            PCA02 = 1,
            PCA03 = 2,
            PCA04 = 3,
            PCA05 = 4,
            PCA06 = 5,
            PCA07 = 6,
            PCA08 = 7,
            PCA09 = 8,
            PCA10 = 9,
            Chin03 = 10,
            Forehead00 = 11,
            Cheeks02 = 12,
            Cheeks01 = 13,
            MouthBag01 = 14,
            MouthBag02 = 15,
            Eyes02 = 16,
            MouthBag03 = 17,
            Forehead04 = 18,
            Nose00 = 19,
            Nose01 = 20,
            Nose02 = 21,
            MouthBag06 = 22,
            MouthBag05 = 23,
            Cheeks00 = 24,
            Mask03 = 25,
            Eyes03 = 26,
            Nose03 = 27,
            Eyes08 = 28,
            MouthBag07 = 29,
            Eyes00 = 30,
            Nose04 = 31,
            Mask04 = 32,
            Chin04 = 33,
            Forehead05 = 34,
            Eyes06 = 35,
            Eyes11 = 36,
            Nose05 = 37,
            Mouth07 = 38,
            Cheeks08 = 39,
            Eyes09 = 40,
            Mask10 = 41,
            Mouth09 = 42,
            Nose07 = 43,
            Nose08 = 44,
            Cheeks07 = 45,
            Mask07 = 46,
            MouthBag09 = 47,
            Nose06 = 48,
            Chin02 = 49,
            Eyes07 = 50,
            Cheeks10 = 51,
            Rim20 = 52,
            Mask22 = 53,
            MouthBag15 = 54,
            Chin01 = 55,
            Cheeks04 = 56,
            Eyes17 = 57,
            Cheeks13 = 58,
            Mouth02 = 59,
            MouthBag12 = 60,
            Mask19 = 61,
            Mask20 = 62,
            Forehead06 = 63,
            Mouth13 = 64,
            Mask25 = 65,
            Chin05 = 66,
            Cheeks20 = 67,
            Nose09 = 68,
            Nose10 = 69,
            MouthBag27 = 70,
            Mouth11 = 71,
            Cheeks14 = 72,
            Eyes16 = 73,
            Mask29 = 74,
            Nose15 = 75,
            Cheeks11 = 76,
            Mouth16 = 77,
            Eyes19 = 78,
            Mouth17 = 79,
            MouthBag36 = 80,
            Mouth15 = 81,
            Cheeks25 = 82,
            Cheeks16 = 83,
            Cheeks18 = 84,
            Rim07 = 85,
            Nose13 = 86,
            Mouth18 = 87,
            Cheeks19 = 88,
            Rim21 = 89,
            Mouth22 = 90,
            Nose18 = 91,
            Nose16 = 92,
            Rim22 = 93,
            Count = (Rim22 + 1)
        };

        public enum FaceAlignmentQuality
        {
            High = 0,
            Low = 1
        };

        public enum FaceModelBuilderCollectionStatus
        {
            Complete = 0,
            MoreFramesNeeded = 0x1,
            FrontViewFramesNeeded = 0x2,
            LeftViewsNeeded = 0x4,
            RightViewsNeeded = 0x8,
            TiltedUpViewsNeeded = 0x10
        };

        public enum FaceModelBuilderCaptureStatus
        {
            GoodFrameCapture = 0,
            OtherViewsNeeded = 1,
            LostFaceTrack = 2,
            FaceTooFar = 3,
            FaceTooNear = 4,
            MovingTooFast = 5,
            SystemError = 6
        };

        public enum FaceModelBuilderAttributes
        {
            None = 0,
            SkinColor = 0x1,
            HairColor = 0x2
        };

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct RectI : IEquatable<RectI>
        {
            public Int32 Left;
            public Int32 Top;
            public Int32 Right;
            public Int32 Bottom;

            public static bool operator !=(RectI a, RectI b)
            {
                return !(a == b);
            }
            public static bool operator ==(RectI a, RectI b)
            {
                if (object.ReferenceEquals(a, b))
                    return true;

                if (((object)a == null) || ((object)b == null))
                    return false;

                return a.Left == b.Left && a.Top == b.Top && a.Right == b.Right && a.Bottom == b.Bottom;
            }
            public override bool Equals(object other)
            {
                if (other == null)
                    return false;

                if (other.GetType() == typeof(RectI))
                    return this == (RectI)other;

                return false;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
            public bool Equals(RectI rect)
            {
                if ((object)rect == null)
                    return false;

                return Left == rect.Left && Top == rect.Top && Right == rect.Right && Bottom == rect.Bottom;
            }

        };

        [Guid("3A348391-43AD-F7D8-437D-D790FA5833DE")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceFrameSource
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsActive();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrameReader OpenReader();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IKinectSensor get_KinectSensor();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeTrackingIdLost();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeTrackingIdLost(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            ITrackingIdLostEventArgs GetTrackingIdLostEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_TrackingId(UInt64 trackingId);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsTrackingIdValid();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            DWORD get_FaceFrameFeatures();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_FaceFrameFeatures(DWORD faceFrameFeatures);

        };

        [Guid("AD0D735F-4E42-82BB-5C03-1BBE5E24CB19")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceFrameReader
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeFrameArrived();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrame AcquireLatestFrame();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsPaused();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_IsPaused(bool isPaused);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrameSource get_FaceFrameSource();

        };

        [Guid("78E06811-4C53-A4BD-4CE7-8C995CAD882F")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceFrameReference
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrame AcquireFrame();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            TIMESPAN get_RelativeTime();

        };

        [Guid("C270AC82-4C03-3BC6-F13E-E680AE7964DA")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceFrameArrivedEventArgs
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrameReference get_FrameReference();

        };

        [Guid("5A59B4D1-489E-7211-9676-44AF6E22E247")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceFrame
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrameResult get_FaceFrameResult();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsTrackingIdValid();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            TIMESPAN get_RelativeTime();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceFrameSource get_FaceFrameSource();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IBodyFrameReference get_BodyFrameReference();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IColorFrameReference get_ColorFrameReference();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IDepthFrameReference get_DepthFrameReference();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IInfraredFrameReference get_InfraredFrameReference();

        };

        [Guid("8873C3F1-445F-BD37-404C-ED92C2906523")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceFrameResult
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            TIMESPAN get_RelativeTime();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            DWORD get_FaceFrameFeatures();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            Vector4 get_FaceRotationQuaternion();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            RectI get_FaceBoundingBoxInColorSpace();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            RectI get_FaceBoundingBoxInInfraredSpace();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void GetFacePointsInColorSpace(
                [In] uint capacity,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] PointF[] facePoints);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void GetFacePointsInInfraredSpace(
                [In] uint capacity,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] PointF[] facePoints);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void GetFaceProperties(
                [In] uint capacity,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] DetectionResult[] detectionResults);

        };

        [Guid("1EFE99CA-4356-FED8-D564-BDA0CB6F15C4")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface ITrackingIdLostEventArgs
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

        };

        [Guid("B48D57AD-A414-4542-BE89-C4898816A678")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface ICollectionStatusChangedEventArgs
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            FaceModelBuilderCollectionStatus get_PreviousCollectionStatus();

        };

        [Guid("BD8BA0B8-6CD5-4051-9246-01A15D023BA9")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface ICaptureStatusChangedEventArgs
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            FaceModelBuilderCaptureStatus get_PreviousCaptureStatus();

        };

        [Guid("38DFFDE8-2C4D-476D-B1DF-43EF4529F2AF")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceAlignment
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            Vector4 get_FaceOrientation();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_FaceOrientation(Vector4 faceOrientation);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            CameraSpacePoint get_HeadPivotPoint();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_HeadPivotPoint(CameraSpacePoint headPivotPoint);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            RectI get_FaceBoundingBox();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void GetAnimationUnits(
                [In] uint capacity,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] float[] animationUnits);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            FaceAlignmentQuality get_Quality();

        };

        [Guid("7B28C340-B6D0-44AC-8CF0-2D3F568C7D7E")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceModel
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            float get_Scale();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt32 get_SkinColor();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt32 get_HairColor();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void GetFaceShapeDeformations(
                [In] uint capacity,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] float[] faceShapeDeformations);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void CalculateVerticesForAlignment(
                [In, MarshalAs(UnmanagedType.Interface)] IFaceAlignment faceAlignment,
                [In] uint capacity,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] CameraSpacePoint[] vertices);

        };

        [Guid("BD92A663-1D1E-4CCF-B202-66DD49834BBE")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceModelBuilder
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeCollectionStatusChanged();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeCollectionStatusChanged(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            ICollectionStatusChangedEventArgs GetCollectionStatusChangedEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeCaptureStatusChanged();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeCaptureStatusChanged(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            ICaptureStatusChangedEventArgs GetCaptureStatusChangedEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            FaceModelBuilderCollectionStatus get_CollectionStatus();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            FaceModelBuilderCaptureStatus get_CaptureStatus();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void BeginFaceDataCollection();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceModelData GetFaceData();

        };

        [Guid("ABD8D92D-E579-466E-BD9E-232E0134BCE0")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IFaceModelData
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceModel ProduceFaceModel();

        };

        [Guid("16806807-70B9-4D0C-A74B-E807783969C3")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IHighDefinitionFaceFrameReference
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IHighDefinitionFaceFrame AcquireFrame();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            TIMESPAN get_RelativeTime();

        };

        [Guid("9B0BAB99-4EC6-4CAB-83F4-3A3C9D864333")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IHighDefinitionFaceFrameArrivedEventArgs
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IHighDefinitionFaceFrameReference get_FrameReference();

        };

        [Guid("E6F33685-BEFC-48CA-B81A-B6A8A79C32FE")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IHighDefinitionFaceFrameSource
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeTrackingIdLost();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeTrackingIdLost(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            ITrackingIdLostEventArgs GetTrackingIdLostEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsOnline();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_IsOnline(bool isOnline);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsActive();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_TrackingId(UInt64 trackingId);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsTrackingIdValid();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            FaceAlignmentQuality get_TrackingQuality();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_TrackingQuality(FaceAlignmentQuality trackingQuality);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceModel get_FaceModel();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_FaceModel(IFaceModel faceModel);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IHighDefinitionFaceFrameReader OpenReader();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceModelBuilder OpenModelBuilder(FaceModelBuilderAttributes enabledAttributes);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IKinectSensor get_KinectSensor();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void FeedAndCalculateFrameData(
                [In, MarshalAs(UnmanagedType.Interface)] IBody body,
                [In] uint infraredCapacity,
                [In] IntPtr /*UInt16[]*/ infraredFrameBuffer,
                uint colorCapacity,
                [In] IntPtr /*byte[]*/ colorFrameBuffer,
                uint depthCapacity,
                [In] IntPtr /*UInt16[]*/ depthFrameBuffer);

        };


        [Guid("FAB67369-E051-4790-8A1D-1CEDB9DE86CA")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IHighDefinitionFaceFrameReader
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeFrameArrived();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IHighDefinitionFaceFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IHighDefinitionFaceFrame AcquireLatestFrame();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsPaused();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_IsPaused(bool isPaused);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IHighDefinitionFaceFrameSource get_HighDefinitionFaceFrameSource();

        };

        [Guid("303A5D6F-1825-41D0-BA14-647FDC7D8A89")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IHighDefinitionFaceFrame
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            TIMESPAN get_RelativeTime();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IHighDefinitionFaceFrameSource get_HighDefinitionFaceFrameSource();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IBodyFrameReference get_BodyFrameReference();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IColorFrameReference get_ColorFrameReference();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IDepthFrameReference get_DepthFrameReference();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IInfraredFrameReference get_InfraredFrameReference();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsTrackingIdValid();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsFaceTracked();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IFaceModel get_FaceModel();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            FaceAlignmentQuality get_FaceAlignmentQuality();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void GetAndRefreshFaceAlignmentResult(IFaceAlignment faceAlignmentResults);

        };
    }
}
