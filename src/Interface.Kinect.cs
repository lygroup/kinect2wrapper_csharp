﻿/*
 Wrap Interface of Kinect2 SDK
 According to MS Kinect SDK Vesrion 2.0.1410.19000

 Date : 2014/12/3
 Version : 0.5
 
The MIT License (MIT)

Copyright (c) 2014 DavidWTF

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Reflection;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;

namespace Kinect2
{
    using WAITABLE_HANDLE = System.IntPtr;
    using TIMESPAN = System.Int64;
    using HRESULT = System.UInt32;
    using DWORD = System.UInt32;

    public class Kinect
    {
        public const int BODY_COUNT = 6;
        public const int DEPTH_WIDTH = 512;
        public const int DEPTH_HEIGHT = 424;
        public const int COLOR_WIDTH = 1920;
        public const int COLOR_HEIGHT = 1080;

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT GetDefaultKinectSensor(
            [Out, MarshalAs(UnmanagedType.Interface)] out IKinectSensor defaultKinectSensor);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT GetKinectCoreWindowForCurrentThread(
            [Out, MarshalAs(UnmanagedType.Interface)] out IKinectCoreWindow kinectCoreWindow);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT CreateKinectGestureRecognizer(
            [Out, MarshalAs(UnmanagedType.Interface)] out IKinectGestureRecognizer kinectGestureRecognizer);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT OverrideKinectInteractionMode(KinectInteractionMode mode);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT SetKinectOnePersonSystemEngagement();

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT SetKinectTwoPersonSystemEngagement();

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT SetKinectOnePersonManualEngagement(
            [In] IBodyHandPair bodyHandPair);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT SetKinectTwoPersonManualEngagement(
            [In] IBodyHandPair bodyHandPair, [In] IBodyHandPair bodyHandPair2);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT GetKinectEngagementMode(
            [Out] out KinectEngagementMode mode);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT GetKinectManualEngagedHandCount(
            [Out] out UInt32 manualEngagedHandCount);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT GetKinectManualEngagedHand(
            [In]UInt32 manualEngagedHandIndex,
            [Out, MarshalAs(UnmanagedType.Interface)] out IBodyHandPair manualEngagedHand);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT GetMaximumKinectEngagedPersonCount(
            [Out] out UInt32 maximumKinectEngagedPersonCount);

        [DllImportAttribute(@"Kinect20.dll")]
        public static extern HRESULT CreateBodyHandPair(
            [In] UInt64 bodyTrackingId,
            [In] HandType handType,
            [Out, MarshalAs(UnmanagedType.Interface)] out IBodyHandPair ppBodyHandPair);

    }

    public enum KinectCapabilities
    {
        None = 0,
        Vision = 0x1,
        Audio = 0x2,
        Face = 0x4,
        Expressions = 0x8,
        Gamechat = 0x10
    };

    public enum FrameSourceTypes
    {
        None = 0,
        Color = 0x1,
        Infrared = 0x2,
        LongExposureInfrared = 0x4,
        Depth = 0x8,
        BodyIndex = 0x10,
        Body = 0x20,
        Audio = 0x40
    };

    public enum ColorImageFormat
    {
        None = 0,
        Rgba = 1,
        Yuv = 2,
        Bgra = 3,
        Bayer = 4,
        Yuy2 = 5
    };

    public enum HandState
    {
        Unknown = 0,
        NotTracked = 1,
        Open = 2,
        Closed = 3,
        Lasso = 4
    };

    public enum Expression
    {
        Neutral = 0,
        Happy = 1,
        Count = (Happy + 1)
    } ;

    public enum DetectionResult
    {
        Unknown = 0,
        No = 1,
        Maybe = 2,
        Yes = 3
    };

    public enum TrackingConfidence
    {
        Low = 0,
        High = 1
    };

    public enum Activity
    {
        EyeLeftClosed = 0,
        EyeRightClosed = 1,
        MouthOpen = 2,
        MouthMoved = 3,
        LookingAway = 4,
        Count = (LookingAway + 1)
    };

    public enum Appearance
    {
        WearingGlasses = 0,
        Count = (WearingGlasses + 1)
    };

    public enum JointType
    {
        SpineBase = 0,
        SpineMid = 1,
        Neck = 2,
        Head = 3,
        ShoulderLeft = 4,
        ElbowLeft = 5,
        WristLeft = 6,
        HandLeft = 7,
        ShoulderRight = 8,
        ElbowRight = 9,
        WristRight = 10,
        HandRight = 11,
        HipLeft = 12,
        KneeLeft = 13,
        AnkleLeft = 14,
        FootLeft = 15,
        HipRight = 16,
        KneeRight = 17,
        AnkleRight = 18,
        FootRight = 19,
        SpineShoulder = 20,
        HandTipLeft = 21,
        ThumbLeft = 22,
        HandTipRight = 23,
        ThumbRight = 24,
        Count = (ThumbRight + 1)
    };

    public enum TrackingState
    {
        NotTracked = 0,
        Inferred = 1,
        Tracked = 2
    };

    public enum FrameEdges
    {
        None = 0,
        Right = 0x1,
        Left = 0x2,
        Top = 0x4,
        Bottom = 0x8
    };

    public enum FrameCapturedStatus
    {
        Unknown = 0,
        Queued = 1,
        Dropped = 2
    };

    public enum AudioBeamMode
    {
        Automatic = 0,
        Manual = 1
    };

    public enum KinectAudioCalibrationState
    {
        Unknown = 0,
        CalibrationRequired = 1,
        Calibrated = 2
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct Vector4
    {
        public float x;
        public float y;
        public float z;
        public float w;
        public float X
        {
            get { return x; }
            set { x = value; }
        }
        public float Y
        {
            get { return y; }
            set { y = value; }
        }
        public float Z
        {
            get { return z; }
            set { z = value; }
        }
        public float W
        {
            get { return w; }
            set { w = value; }
        }
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct PointF
    {
        public float X;
        public float Y;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct RectF
    {
        public float X;
        public float Y;
        public float Width;
        public float Height;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct ColorSpacePoint
    {
        public float X;
        public float Y;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct DepthSpacePoint
    {
        public float X;
        public float Y;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct CameraSpacePoint
    {
        public float X;
        public float Y;
        public float Z;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct Joint
    {
        public JointType JointType;
        public CameraSpacePoint Position;
        public TrackingState TrackingState;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct JointOrientation
    {
        public JointType JointType;
        public Vector4 Orientation;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct CameraIntrinsics
    {
        public float FocalLengthX;
        public float FocalLengthY;
        public float PrincipalPointX;
        public float PrincipalPointY;
        public float RadialDistortionSecondOrder;
        public float RadialDistortionFourthOrder;
        public float RadialDistortionSixthOrder;
    };

    [Guid("3C6EBA94-0DE1-4360-B6D4-653A10794C8B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectSensor
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeIsAvailableChanged();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeIsAvailableChanged(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IIsAvailableChangedEventArgs GetIsAvailableChangedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void Open();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void Close();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsOpen();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsAvailable();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrameSource get_ColorFrameSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrameSource get_DepthFrameSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrameSource get_BodyFrameSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrameSource get_BodyIndexFrameSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrameSource get_InfraredFrameSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrameSource get_LongExposureInfraredFrameSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioSource get_AudioSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IMultiSourceFrameReader OpenMultiSourceFrameReader(DWORD enabledFrameSourceTypes);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ICoordinateMapper get_CoordinateMapper();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void get_UniqueKinectId(
            [In] uint bufferSize,
            [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] char[] uniqueKinectId);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        DWORD get_KinectCapabilities();

    };

    [Guid("3A6DD52E-967F-4982-B3D9-74B9E1A044C9")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IIsAvailableChangedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsAvailable();

    };

    [Guid("21F6EFB7-EB6D-48F4-9C08-181A87BF0C98")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IFrameDescription
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        int get_Width();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        int get_Height();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_HorizontalFieldOfView();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_VerticalFieldOfView();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_DiagonalFieldOfView();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        uint get_LengthInPixels();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        uint get_BytesPerPixel();

    };

    [Guid("24CBAB8E-DF1A-4FA8-827E-C1B27A44A3A1")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IFrameCapturedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        FrameSourceTypes get_FrameType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        FrameCapturedStatus get_FrameStatus();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("C0F6432B-9FFE-4AB3-A683-F37C72BBB158")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IMultiSourceFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeMultiSourceFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeMultiSourceFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IMultiSourceFrameArrivedEventArgs GetMultiSourceFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IMultiSourceFrame AcquireLatestFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        DWORD get_FrameSourceTypes();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

    };

    [Guid("3532F40B-D908-451D-BBF4-6CA73B782558")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IMultiSourceFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IMultiSourceFrameReference get_FrameReference();

    };

    [Guid("DD70E845-E283-4DD1-8DAF-FC259AC5F9E3")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IMultiSourceFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IMultiSourceFrame AcquireFrame();

    };

    [Guid("29A63AFB-76CE-4359-895A-997F1E094D1C")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IMultiSourceFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrameReference get_ColorFrameReference();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrameReference get_DepthFrameReference();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrameReference get_BodyFrameReference();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrameReference get_BodyIndexFrameReference();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrameReference get_InfraredFrameReference();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrameReference get_LongExposureInfraredFrameReference();

    };

    [Guid("5CC49E38-9BBD-48BE-A770-FD30EA405247")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IColorFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrame AcquireFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("82A2E32F-4AE5-4614-88BB-DCC5AE0CEAED")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IColorFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrameReference get_FrameReference();

    };

    [Guid("57621D82-D8EE-4783-B412-F7E019C96CFD")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IColorFrameSource
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameCaptured();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameCaptured(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameCapturedEventArgs GetFrameCapturedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrameReader OpenReader();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription CreateFrameDescription(ColorImageFormat format);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

    };

    [Guid("9BEA498C-C59C-4653-AAF9-D884BAB7C35B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IColorFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrame AcquireLatestFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrameSource get_ColorFrameSource();

    };

    [Guid("39D05803-8803-4E86-AD9F-13F6954E4ACA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IColorFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        ColorImageFormat get_RawColorImageFormat();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CopyRawFrameDataToArray(
            [In] uint capacity,
            //[Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] byte[] frameData);
            [Out] IntPtr /*byte[]*/frameData);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void AccessRawUnderlyingBuffer(
            [Out] out uint capacity,
            [Out] out IntPtr /*byte[]*/ buffer);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CopyConvertedFrameDataToArray(
            [In] uint capacity,
            //[Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] byte[] frameData,
            [Out] IntPtr /*byte[]*/frameData,
            [In] ColorImageFormat colorFormat);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription CreateFrameDescription(ColorImageFormat format);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        IColorCameraSettings get_ColorCameraSettings();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IColorFrameSource get_ColorFrameSource();

    };

    [Guid("20621E5E-ABC9-4EBD-A7EE-4C77EDD0152A")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IDepthFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrame AcquireFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("2B01BCB8-29D7-4726-860C-6DA56664AA81")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IDepthFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrameReference get_FrameReference();

    };

    [Guid("C428D558-5E46-490A-B699-D1DDDAA24150")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IDepthFrameSource
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameCaptured();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameCaptured(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameCapturedEventArgs GetFrameCapturedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrameReader OpenReader();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt16 get_DepthMinReliableDistance();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt16 get_DepthMaxReliableDistance();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

    };

    [Guid("81C0C0AB-6E6C-45CB-8625-A5F4D38759A4")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IDepthFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrame AcquireLatestFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrameSource get_DepthFrameSource();

    };

    [Guid("D8600853-8835-44F9-84A7-E617CDD7DFDD")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IDepthFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CopyFrameDataToArray(
            [In] uint capacity,
            //[Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] UInt16[] frameData);
            [Out] IntPtr /*UInt16[]*/ frameData);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void AccessUnderlyingBuffer(
            [Out] out uint capacity,
            [Out] out IntPtr /*UInt16[]*/ buffer);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IDepthFrameSource get_DepthFrameSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        ushort get_DepthMinReliableDistance();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        ushort get_DepthMaxReliableDistance();

    };

    [Guid("C3A1733C-5F84-443B-9659-2F2BE250C97D")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrame AcquireFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("BF5CCA0E-00C1-4D48-837F-AB921E6AEE01")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrameReference get_FrameReference();
    };

    [Guid("BB94A78A-458C-4608-AC69-34FEAD1E3BAE")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyFrameSource
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameCaptured();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameCaptured(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameCapturedEventArgs GetFrameCapturedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        int get_BodyCount();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrameReader OpenReader();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OverrideHandTracking(UInt64 trackingId);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OverrideAndReplaceHandTracking(UInt64 oldTrackingId, UInt64 newTrackingId);

    };

    [Guid("45532DF5-A63C-418F-A39F-C567936BC051")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrame AcquireLatestFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrameSource get_BodyFrameSource();

    };

    [Guid("52884F1F-94D7-4B57-BF87-9226950980D5")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetAndRefreshBodyData(
            [In] uint capacity,
            [In, Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] IntPtr[] /*IBody[]*/ bodies);


        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        Vector4 get_FloorClipPlane();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyFrameSource get_BodyFrameSource();

    };

    [Guid("46AEF731-98B0-4D18-827B-933758678F4A")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBody
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetJoints(
            [In] uint capacity,
            [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] Joint[] joints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetJointOrientations(
            [In] uint capacity,
            [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] JointOrientation[] jointOrientations);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        DetectionResult get_Engaged();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetExpressionDetectionResults(
            [In] uint capacity,
            [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] DetectionResult[] detectionResults);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetActivityDetectionResults(
            [In] uint capacity,
            [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] DetectionResult[] detectionResults);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetAppearanceDetectionResults(
            [In] uint capacity,
            [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] DetectionResult[] detectionResults);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        HandState get_HandLeftState();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TrackingConfidence get_HandLeftConfidence();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        HandState get_HandRightState();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TrackingConfidence get_HandRightConfidence();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        DWORD get_ClippedEdges();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt64 get_TrackingId();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsTracked();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsRestricted();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Lean();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TrackingState get_LeanTrackingState();

    };

    [Guid("D0EA0519-F7E7-4B1E-B3D8-03B3C002795F")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyIndexFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrame AcquireFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("10B7E92E-B4F2-4A36-A459-06B2A4B249DF")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyIndexFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrameReference get_FrameReference();

    };

    [Guid("010F2A40-DC58-44A5-8E57-329A583FEC08")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyIndexFrameSource
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameCaptured();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameCaptured(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameCapturedEventArgs GetFrameCapturedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrameReader OpenReader();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

    };

    [Guid("E9724AA1-EBFA-48F8-9044-E0BE33383B8B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyIndexFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrame AcquireLatestFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrameSource get_BodyIndexFrameSource();

    };

    [Guid("2CEA0C07-F90C-44DF-A18C-F4D18075EA6B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyIndexFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CopyFrameDataToArray(
            [In] uint capacity,
            //[Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] byte[] frameData);
            [Out] IntPtr /*byte[]*/ frameData);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void AccessUnderlyingBuffer(
            [Out] out uint capacity,
            [Out] out IntPtr /*byte[]*/ buffer);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IBodyIndexFrameSource get_BodyIndexFrameSource();

    };

    [Guid("60183D5B-DED5-4D5C-AE59-64C7724FE5FE")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IInfraredFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrame AcquireFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("7E17F78E-D9D1-4448-90C2-4E50EC4ECEE9")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IInfraredFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrameReference get_FrameReference();

    };

    [Guid("4C299EC6-CA45-4AFF-87AD-DF5762C49BE7")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IInfraredFrameSource
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameCaptured();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameCaptured(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameCapturedEventArgs GetFrameCapturedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrameReader OpenReader();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

    };

    [Guid("059A049D-A0AC-481E-B342-483EE94A028B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IInfraredFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrame AcquireLatestFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrameSource get_InfraredFrameSource();

    };

    [Guid("EA83823C-7613-4F29-BD51-4A9678A52C7E")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IInfraredFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CopyFrameDataToArray(
            [In] uint capacity,
            //[Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] UInt16[] frameData);
            [Out] IntPtr /*UInt16[]*/ frameData);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void AccessUnderlyingBuffer(
            [Out] out uint capacity,
            [Out] out IntPtr /*UInt16[]*/ buffer);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IInfraredFrameSource get_InfraredFrameSource();

    };

    [Guid("10043A3E-0DAA-409C-9944-A6FC66C85AF7")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface ILongExposureInfraredFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrame AcquireFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("D73D4B5E-E329-4F04-894C-0C97482690D4")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface ILongExposureInfraredFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrameReference get_FrameReference();

    };

    [Guid("D7150EDA-EDA2-4673-B4F8-E3C76D1F402B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface ILongExposureInfraredFrameSource
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameCaptured();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameCaptured(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameCapturedEventArgs GetFrameCapturedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrameReader OpenReader();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

    };

    [Guid("2AF23594-0115-417B-859F-A0E3FFB690D2")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface ILongExposureInfraredFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrame AcquireLatestFrame();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrameSource get_LongExposureInfraredFrameSource();

    };

    [Guid("D1199394-9A42-4577-BE12-90A38B72282C")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface ILongExposureInfraredFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CopyFrameDataToArray(
            [In] uint capacity,
            //[Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] UInt16[] frameData);
            [Out] IntPtr /*UInt16[]*/ frameData);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void AccessUnderlyingBuffer(
            [Out] out uint capacity,
            [Out] out IntPtr /*UInt16[]*/ buffer);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameDescription get_FrameDescription();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ILongExposureInfraredFrameSource get_LongExposureInfraredFrameSource();

    };

    [Guid("F692D23A-14D0-432D-B802-DD381A45A121")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeam
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioSource get_AudioSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        AudioBeamMode get_AudioBeamMode();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_AudioBeamMode(AudioBeamMode audioBeamMode);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_BeamAngle();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_BeamAngle(float beamAngle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_BeamAngleConfidence();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IStream OpenInputStream();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };


    [Guid("3C792C7B-7D95-4C56-9DC7-EF63955781EA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeamList
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        uint get_BeamCount();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OpenAudioBeam(
            [In] UInt32 index,
            [Out, MarshalAs(UnmanagedType.Interface)] out IAudioBeam audioBeam);

    };

    [Guid("5393C8B9-C044-49CB-BDD6-23DFFFD7427E")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeamFrameList
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        uint get_BeamCount();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OpenAudioBeamFrame(
            [In] UInt32 index,
            [Out, MarshalAs(UnmanagedType.Interface)] out IAudioBeamFrame audioBeamFrame);

    };

    [Guid("07AADCC8-EC4A-42F8-90A9-C72ECF0A1D06")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeamFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioSource get_AudioSource();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_Duration();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioBeam get_AudioBeam();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt32 get_SubFrameCount();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetSubFrame(
            [In] UInt32 subFrameIndex,
            [Out, MarshalAs(UnmanagedType.Interface)] out IAudioBeamSubFrame audioBeamSubFrame);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTimeStart();

    };

    [Guid("0967DB97-80D1-4BC5-BD2B-4685098D9795")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeamSubFrame
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        uint get_FrameLengthInBytes();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_Duration();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_BeamAngle();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_BeamAngleConfidence();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        AudioBeamMode get_AudioBeamMode();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt32 get_AudioBodyCorrelationCount();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void GetAudioBodyCorrelation(
            [In] uint index,
            [Out, MarshalAs(UnmanagedType.Interface)] out IAudioBodyCorrelation ppAudioBodyCorrelation);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CopyFrameDataToArray(
            [In] uint capacity,
            //[Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] byte[] frameData);
            [Out] IntPtr /*byte[] */ frameData);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void AccessUnderlyingBuffer(
            [Out] out uint capacity,
            [Out] out IntPtr /*byte[]*/ buffer);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("1BD29D0E-6304-4AFB-9C85-77CFE3DC4FCE")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeamFrameReference
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioBeamFrameList AcquireBeamFrames();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_RelativeTime();

    };

    [Guid("C5BA2355-07DB-47C3-ABC4-68D24B91DE61")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBodyCorrelation
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt64 get_BodyTrackingId();

    };

    [Guid("E0DBE62D-2045-4571-8D1D-ECF3981E3C3D")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeamFrameArrivedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioBeamFrameReference get_FrameReference();

    };

    [Guid("B5733DE9-6ECF-46B2-8B23-A16D71F1A75C")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioBeamFrameReader
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameArrived();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioBeamFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioBeamFrameList AcquireLatestBeamFrames();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPaused();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_IsPaused(bool isPaused);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioSource get_AudioSource();

    };

    [Guid("52D1D743-AED1-4E61-8AF8-19EF287A662C")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IAudioSource
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeFrameCaptured();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeFrameCaptured(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IFrameCapturedEventArgs GetFrameCapturedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectSensor get_KinectSensor();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        uint get_SubFrameLengthInBytes();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_SubFrameDuration();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        uint get_MaxSubFrameCount();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioBeamFrameReader OpenReader();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IAudioBeamList get_AudioBeams();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectAudioCalibrationState get_AudioCalibrationState();

    };

    [Guid("8784DF2D-16B0-481C-A11E-55E70BF25018")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface ICoordinateMapper
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribeCoordinateMappingChanged();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribeCoordinateMappingChanged(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        ICoordinateMappingChangedEventArgs GetCoordinateMappingChangedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        DepthSpacePoint MapCameraPointToDepthSpace(CameraSpacePoint cameraPoint);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        ColorSpacePoint MapCameraPointToColorSpace(CameraSpacePoint cameraPoint);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        CameraSpacePoint MapDepthPointToCameraSpace(DepthSpacePoint depthPoint, UInt16 depth);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        ColorSpacePoint MapDepthPointToColorSpace(DepthSpacePoint depthPoint, UInt16 depth);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapCameraPointsToDepthSpace(
            [In] uint cameraPointCount,
            [In] IntPtr /*CameraSpacePoint[]*/ cameraPoints,
            [In] uint depthPointCount,
            [Out] IntPtr /*DepthSpacePoint[]*/ depthPoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapCameraPointsToColorSpace(
            [In] uint cameraPointCount,
            [In] IntPtr /*CameraSpacePoint[]*/ cameraPoints,
            [In] uint colorPointCount,
            [Out] IntPtr /*ColorSpacePoint[]*/ colorPoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapDepthPointsToCameraSpace(
            [In] uint depthPointCount,
            [In] IntPtr /*DepthSpacePoint[]*/ depthPoints,
            [In] uint depthCount,
            [In] IntPtr /*UInt16[]*/ depths,
            [In] uint cameraPointCount,
            [Out] IntPtr /*CameraSpacePoint[]*/ cameraPoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapDepthPointsToColorSpace(
            [In] uint depthPointCount,
            [In] IntPtr /*DepthSpacePoint[]*/ depthPoints,
            [In] uint depthCount,
            [In] IntPtr /*UInt16[]*/ depths,
            [In] uint colorPointCount,
            [Out] IntPtr /*ColorSpacePoint[]*/ colorPoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapDepthFrameToCameraSpace(
            [In] uint depthPointCount,
            [In] IntPtr /*UInt16[]*/ depthFrameData,
            [In] uint cameraPointCount,
            [Out] IntPtr /*CameraSpacePoint[]*/ cameraSpacePoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapDepthFrameToColorSpace(
            [In] uint depthPointCount,
            [In] IntPtr /*UInt16[]*/ depthFrameData,
            [In] uint colorPointCount,
            [Out] IntPtr /*ColorSpacePoint[]*/ colorSpacePoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapColorFrameToDepthSpace(
            [In] uint depthDataPointCount,
            [In] IntPtr /*UInt16[]*/ depthFrameData,
            [In] uint depthPointCount,
            [Out] IntPtr /*DepthSpacePoint[]*/ depthSpacePoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void MapColorFrameToCameraSpace(
            [In] uint depthDataPointCount,
            [In] IntPtr /*UInt16[]*/ depthFrameData,
            [In] uint cameraPointCount,
            [Out] IntPtr /*CameraSpacePoint[]*/ cameraSpacePoints);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)]
        PointF[] GetDepthFrameToCameraSpaceTable([Out] out UInt32 tableEntryCount);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        CameraIntrinsics GetDepthCameraIntrinsics();

    };

    [Guid("E9A2A0BF-13BD-4A53-A157-91FC8BB41F85")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface ICoordinateMappingChangedEventArgs
    {

    };

    [Guid("DBF802AB-0ADF-485A-A844-CF1C7956D039")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IColorCameraSettings
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_ExposureTime();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_FrameInterval();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_Gain();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_Gamma();

    };

    public enum PointerDeviceType
    {
        Touch = 0,
        Pen = 1,
        Mouse = 2,
        Kinect = 3
    };

    public enum HandType
    {
        NONE = 0,
        LEFT = (NONE + 1),
        RIGHT = (LEFT + 1)
    };

    public enum KinectHoldingState
    {
        Started = 0,
        Completed = 1,
        Canceled = 2
    };

    public enum KinectGestureSettings
    {
        None = 0,
        Tap = 0x1,
        ManipulationTranslateX = 0x40,
        ManipulationTranslateY = 0x80,
        ManipulationTranslateRailsX = 0x100,
        ManipulationTranslateRailsY = 0x200,
        ManipulationScale = 0x800,
        ManipulationTranslateInertia = 0x1000,
        KinectHold = 0x10000
    };

    public enum KinectInteractionMode
    {
        Normal = 0,
        Off = 1,
        Media = 2
    };

    public enum KinectEngagementMode
    {
        None = 0,
        SystemOnePerson = 1,
        SystemTwoPerson = 2,
        ManualOnePerson = 3,
        ManualTwoPerson = 4
    } ;

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct KinectManipulationDelta
    {
        public PointF Translation;
        public float Scale;
        public float Rotation;
        public float Expansion;
    };

    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct KinectManipulationVelocities
    {
        public PointF Linear;
        public float Angular;
        public float Expansion;
    };

    [Guid("3F0C2E40-E9DC-4178-A8F5-5600E059C84C")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IBodyHandPair
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt64 get_BodyTrackingId();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_BodyTrackingId(UInt64 value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        HandType get_HandType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_HandType(HandType value);

    };

    [Guid("6B5DDAC2-1C7A-4962-937B-A7A527149C64")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectCoreWindow
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribePointerEntered();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribePointerEntered(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectPointerEventArgs GetPointerEnteredEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribePointerMoved();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribePointerMoved(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectPointerEventArgs GetPointerMovedEventData(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        WAITABLE_HANDLE SubscribePointerExited();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnsubscribePointerExited(WAITABLE_HANDLE waitableHandle);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectPointerEventArgs GetPointerExitedEventData(WAITABLE_HANDLE waitableHandle);

    };

    [Guid("5F51A3C4-2400-4F6D-8A6C-25B96715EFF1")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectGestureRecognizer
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterSelectionTappedHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerSelectionHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterSelectionTappedHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterSelectionHoldingHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerSelectionHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterSelectionHoldingHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterSelectionPressingStartedHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerSelectionHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterSelectionPressingStartedHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterSelectionPressingUpdatedHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerSelectionHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterSelectionPressingUpdatedHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterSelectionPressingCompletedHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerSelectionHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterSelectionPressingCompletedHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterManipulationStartedHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerManipulationHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterManipulationStartedHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterManipulationUpdatedHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerManipulationHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterManipulationUpdatedHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterManipulationInertiaStartingHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerManipulationHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterManipulationInertiaStartingHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void RegisterManipulationCompletedHandler(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectGestureRecognizerManipulationHandler handler);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void UnregisterManipulationCompletedHandler();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectGestureSettings get_GestureSettings();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_GestureSettings(KinectGestureSettings value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsInertial();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsActive();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_InertiaTranslationDeceleration();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_InertiaTranslationDeceleration(float value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_InertiaTranslationDisplacement();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_InertiaTranslationDisplacement(float value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_AutoProcessInertia();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_AutoProcessInertia(bool value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        RectF get_BoundingRect();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_BoundingRect(RectF value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void ProcessDownEvent(IKinectPointerPoint value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void ProcessUpEvent(IKinectPointerPoint value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void ProcessMoveEvents(IKinectPointerPoint value);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void ProcessInertia();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void CompleteGesture();

    };

    [Guid("9F3D5C22-158F-4A4E-AD4E-023FAE1F4BC9")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectGestureRecognizerSelectionHandler
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnTapped(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectTappedEventArgs args);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnHolding(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectHoldingEventArgs args);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnPressingStarted(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectPressingStartedEventArgs args);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnPressingUpdated(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectPressingUpdatedEventArgs args);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnPressingCompleted(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectPressingCompletedEventArgs args);

    };


    [Guid("398CF7E0-4FD4-4336-BAAA-B6708B4C437B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectGestureRecognizerManipulationHandler
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnManipulationStarted(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectManipulationStartedEventArgs args);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnManipulationUpdated(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectManipulationUpdatedEventArgs args);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnManipulationInertiaStarting(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectManipulationInertiaStartingEventArgs args);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void OnManipulationCompleted(
            [In, MarshalAs(UnmanagedType.Interface)] IKinectManipulationCompletedEventArgs args);

    };

    [Guid("A7B4B8F4-55B5-4FB2-B7C4-83B7700748DF")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectHoldingEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointerDeviceType get_PointerDeviceType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectHoldingState get_HoldingState();

    };

    [Guid("98AF1727-D511-459C-BE14-16CA8230871B")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectManipulationCompletedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointerDeviceType get_PointerDeviceType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationDelta get_Cumulative();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationVelocities get_Velocities();

    };

    [Guid("90ACC90A-4573-406B-9715-20171D8A3875")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectManipulationInertiaStartingEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointerDeviceType get_PointerDeviceType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationDelta get_Delta();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationDelta get_Cumulative();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationVelocities get_Velocities();

    };

    [Guid("2EC8A1BA-96EC-49F7-BD41-D5FE791F0D46")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectManipulationStartedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointerDeviceType get_PointerDeviceType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationDelta get_Cumulative();

    };

    [Guid("1097A513-F25E-4B48-84F2-2D3B0B3FC78C")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectManipulationUpdatedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointerDeviceType get_PointerDeviceType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationDelta get_Delta();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationDelta get_Cumulative();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        KinectManipulationDelta get_Velocities();

    };

    [Guid("C6F3A13D-16DA-4556-AD25-C4CD73A2D625")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectPointerDevice
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointerDeviceType get_PointerDeviceType();

    };

    [Guid("65635AE1-3D28-4D5A-AA09-825AAE07C1B1")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectPointerEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_Handled();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void put_Handled(bool handled);

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectPointerPoint get_CurrentPoint();

    };

    [Guid("D2F1E5B8-9DAD-4933-BCD2-90F4C64E17AC")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectPointerPoint
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectPointerDevice get_PointerDevice();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt32 get_PointerId();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_RawPosition();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt64 get_Timestamp();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        [return: MarshalAs(UnmanagedType.Interface)]
        IKinectPointerPointProperties get_Properties();

    };

    [Guid("5527A776-CA76-47BA-95B4-A2DB2BD32207")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectPointerPointProperties
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsPrimary();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsInRange();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        bool get_IsEngaged();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt64 get_BodyTrackingId();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        HandType get_HandType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_HandReachExtent();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        TIMESPAN get_BodyTimeCounter();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_HandRotation();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_PressExtent();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_UnclampedPosition();

    };

    [Guid("7B9DAD43-0DFA-48DC-A2E3-CDBA3B3E251F")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectPressingCompletedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

    };

    [Guid("C01511F4-C7B3-4BA1-A15F-09DC854962AB")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectPressingStartedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

    };

    [Guid("D7D93D29-228E-4EBD-A3B9-E1B916B9BAEF")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectPressingUpdatedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_PressExtent();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        float get_HoldProgress();

    };

    [Guid("D7D93D29-228E-4EBD-A3B9-E1B916B9BAEF")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [ComImport()]
    public interface IKinectTappedEventArgs
    {
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointerDeviceType get_PointerDeviceType();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        PointF get_Position();

        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        UInt32 get_TapCount();

    };
}
