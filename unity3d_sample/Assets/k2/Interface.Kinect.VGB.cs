﻿/*
 Wrap Interface of Kinect2 SDK
 According to MS Kinect SDK Vesrion 2.0.1410.19000

 Date : 2014/12/3
 Version : 0.5
 
The MIT License (MIT)

Copyright (c) 2014 DavidWTF

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;

namespace Kinect2
{
    namespace VGB
    {
        using WAITABLE_HANDLE = System.IntPtr;
        using TIMESPAN = System.Int64;
        using HRESULT = System.Int32;

        public class KinectVGB
        {
            [DllImportAttribute("Kinect20.VisualGestureBuilder")]
            public static extern HRESULT CreateVisualGestureBuilderFrameSource(
                [In, MarshalAs(UnmanagedType.Interface)] IKinectSensor sensor,
                [In] UInt64 initialTrackingId,
                [Out, MarshalAs(UnmanagedType.Interface)] out IVisualGestureBuilderFrameSource source);

            [DllImportAttribute("Kinect20.VisualGestureBuilder")]
            public static extern HRESULT CreateVisualGestureBuilderDatabaseInstanceFromFile(
                [In, MarshalAs(UnmanagedType.BStr)] string name,
                [Out, MarshalAs(UnmanagedType.Interface)] out IVisualGestureBuilderDatabase instance);


            [DllImportAttribute("Kinect20.VisualGestureBuilder")]
            public static extern HRESULT CreateVisualGestureBuilderDatabaseInstanceFromMemory(
                [In] uint capacity,
                [In] IntPtr buffer,
                [Out, MarshalAs(UnmanagedType.Interface)] out IVisualGestureBuilderDatabase instance);
        }

        public enum GestureType
        {
            None = 0,
            Discrete = 1,
            Continuous = 2
        };

        [Guid("288C0678-BC9E-4A05-8005-7814601B4338")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface ITrackingIdLostEventArgs
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

        };

        [Guid("DFD279E6-D8BB-49C1-A61B-E4017D22B412")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IGesture
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void get_Name(
                [In] uint bufferSize,
                [Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)]char[] filePath);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            GestureType get_GestureType();

        };

        [Guid("5807207E-3BA7-489B-A9D9-5B91521627FF")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IDiscreteGestureResult
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_Detected();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            float get_Confidence();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_FirstFrameDetected();

        };

        [Guid("074499A2-7144-4E92-8772-B233BF91F3A2")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IContinuousGestureResult
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            float get_Progress();

        };

        [Guid("7FA8E82E-E43E-4DD6-A481-1E967DC4B7C8")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IVisualGestureBuilderDatabase
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            uint get_AvailableGesturesCount();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void get_AvailableGestures(
                    [In]  UInt32 capacity,
                    [Out, MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.Interface, SizeParamIndex = 0)] IGesture[] availableGestures);

        };

        [Guid("51EB9645-BFED-4625-B95E-E8ECF7F16B15")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IVisualGestureBuilderFrameSource
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeTrackingIdLost();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeTrackingIdLost(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            ITrackingIdLostEventArgs GetTrackingIdLostEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsActive();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_TrackingId(UInt64 trackingId);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsTrackingIdValid();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_HorizontalMirror();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_HorizontalMirror(bool horizontalMirror);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            uint get_GestureCount();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void get_Gestures(
                    [In] uint capacity,
                    [In, Out, MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.Interface, SizeParamIndex = 0)] IGesture[] gestures);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void AddGesture(
                    [In, MarshalAs(UnmanagedType.Interface)]IGesture gesture);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void AddGestures(
                    [In] uint capacity,
                    [In, MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.Interface, SizeParamIndex = 0)] IGesture[] gestures);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void RemoveGesture(
                    [In, MarshalAs(UnmanagedType.Interface)]IGesture gesture);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool GetIsEnabled(
                    [In, MarshalAs(UnmanagedType.Interface)]IGesture gesture);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void SetIsEnabled(
                    [In, MarshalAs(UnmanagedType.Interface)]IGesture gesture,
                    [In] bool isEnabled);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IVisualGestureBuilderFrameReader OpenReader();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IKinectSensor get_KinectSensor();

        };

        [Guid("9EC0C4CD-3CB0-4ED6-ABF9-7C7389F852F9")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IVisualGestureBuilderFrameReader
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            WAITABLE_HANDLE SubscribeFrameArrived();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void UnsubscribeFrameArrived(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IVisualGestureBuilderFrameArrivedEventArgs GetFrameArrivedEventData(WAITABLE_HANDLE waitableHandle);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IVisualGestureBuilderFrame CalculateAndAcquireLatestFrame();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsPaused();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            void put_IsPaused(bool isPaused);

            [return: MarshalAs(UnmanagedType.Interface)]
            IVisualGestureBuilderFrameSource get_VisualGestureBuilderFrameSource();

        };

        [Guid("2393DACC-2182-491F-A140-484F4218286D")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IVisualGestureBuilderFrameReference
        {

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IVisualGestureBuilderFrame AcquireFrame();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            TIMESPAN get_RelativeTime();

        };

        [Guid("FBF3F74C-D814-4CD4-BC2E-C7BD0A4BF362")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IVisualGestureBuilderFrameArrivedEventArgs
        {

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IVisualGestureBuilderFrameReference get_FrameReference();

        };

        [Guid("0C00BA06-C6F2-4F75-AA12-EDDBA2018AB8")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [ComImport()]
        public interface IVisualGestureBuilderFrame
        {

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IContinuousGestureResult get_ContinuousGestureResult(
                [In, MarshalAs(UnmanagedType.Interface)] IGesture gesture);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IDiscreteGestureResult get_DiscreteGestureResult(
                [In, MarshalAs(UnmanagedType.Interface)] IGesture gesture);

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            UInt64 get_TrackingId();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            bool get_IsTrackingIdValid();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            TIMESPAN get_RelativeTime();

            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            [return: MarshalAs(UnmanagedType.Interface)]
            IVisualGestureBuilderFrameSource get_VisualGestureBuilderFrameSource();

        };
    }
}
