#kinect2wrapper_csharp
=======================

本项目包装Kinect 2.0 SDK到基本的C#中。
尽管官方SDK提供了C# API, 但是基于.Net 4.5的。
而Unity3D(和其它一些项目)目前只支持.Net 3.5。
所以，在Unity3D中使用SDK的唯一方法是将C++的API接口包装到C#中。

包装分两步完成。
首先，通过DllImport和Marshal将C++的函数和COM接口转换到C#中。
然后，我们用这些函数和接口构建类。
我们尽量保持与官方的API一致。

本项目未完结。欢迎开发者加入。




This project will wrap Kinect 2.0 SDK to bascily C#. 
Also The official SDK provider the C# APIs, but based on .Net 4.5.
While Unity3D (and other projects) only support .Net 3.5 until now. 
So, the only way to use SDK in Unity3D is wrapping C++ APIs into C#. 

The wrapper are made by two steps.
Firstly, we convert C++'s functions and COM interfaces into C# by DllImport and Marshal.
Secondly, we build classes based on these functions and interfaces.
We try to provide the same or similar APIs to the officials.

The project is open. Welcome developers to join us.



